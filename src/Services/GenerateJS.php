<?php

namespace Drupal\ebt_core\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\media\Entity\Media;
use Drupal\media\OEmbed\UrlResolver;
use Drupal\Core\File\FileUrlGeneratorInterface;

/**
 * Transform Block settings in JS.
 */
class GenerateJS {

  /**
   * The EBT Core configuration.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * Converts oEmbed media URLs into endpoint-specific resource URLs.
   *
   * @var \Drupal\media\OEmbed\UrlResolver
   */
  protected $urlResolver;

  /**
   * The file URL generator.
   *
   * @var \Drupal\Core\File\FileUrlGeneratorInterface
   */
  protected $fileUrlGenerator;

  /**
   * Constructs a new GenerateCSS object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory.
   * @param \Drupal\media\OEmbed\UrlResolver $url_resolver
   *   Converts oEmbed media URLs into endpoint-specific resource URLs.
   * @param \Drupal\Core\File\FileUrlGeneratorInterface $file_url_generator
   *   The file URL generator.
   */
  public function __construct(ConfigFactoryInterface $config_factory, UrlResolver $url_resolver, FileUrlGeneratorInterface $file_url_generator = NULL) {
    $this->config = $config_factory->get('ebt_core.settings');
    $this->urlResolver = $url_resolver;
    if (!$file_url_generator && \Drupal::hasService('file_url_generator')) {
      $file_url_generator = \Drupal::service('file_url_generator');
    }
    $this->fileUrlGenerator = $file_url_generator;
  }

  /**
   * Generate JS from $settings.
   */
  public function generateFromSettings($settings) {
    $javascript_data = [];

    if (!empty($settings['other_settings']['background_media'])) {
      $media = Media::load($settings['other_settings']['background_media']);
      if (!empty($media) && $media->bundle() == 'image') {
        /** @var \Drupal\file\Entity\File $file */
        $file = $media->field_media_image->entity;
        $uri = $file->getFileUri();
        if ($this->fileUrlGenerator !== NULL) {
          $media_url = $this->fileUrlGenerator->generateAbsoluteString($uri);
        }
        else {
          // Add support for Drupal 9.2 and below.
          $media_url = file_url_transform_relative(file_create_url($uri));
        }
        if (!empty($media_url) && !empty($settings['other_settings']['background_image_style']) &&
          $settings['other_settings']['background_image_style'] == 'parallax') {
          $javascript_data['ebtCoreParallax']['mediaUrl'] = $media_url;
        }
      }
      elseif (!empty($media) && $media->bundle() == 'remote_video') {
        $javascript_data['ebtCoreBackgroundRemoteVideo']['mediaUrl'] = $media->field_media_oembed_video->value;
        $provider = $this->urlResolver->getProviderByUrl($media->field_media_oembed_video->value);
        if ($provider->getName() == 'YouTube') {
          $javascript_data['ebtCoreBackgroundRemoteVideo']['mediaProvider'] = 'YouTube';
        }
      }
    }

    return $javascript_data;
  }

}
